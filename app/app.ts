import { Component} from '@angular/core';
import { Platform, ionicBootstrap } from 'ionic-angular';
import { disableDeprecatedForms, provideForms } from '@angular/forms';
import { StatusBar } from 'ionic-native';

import { LoginPage } from './pages/login/login';
import { TabsPage } from './pages/tabs/tabs';
import { AuthService } from './providers/auth-service/auth-service';
 
@Component({
  template: '<ion-nav [root]="rootPage"></ion-nav>',
  providers: [AuthService]
})
export class MyApp {

  private rootPage:any;

  constructor(
    private platform:Platform,
    private authService: AuthService) {
    
    this.detectRootPage();

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });
  }

  // detect what page to show
  detectRootPage() {
   
    this.authService.check()
      .then(res => {
        if(res) {
          this.rootPage = TabsPage;
        } else {
           this.rootPage = LoginPage;
        }
      })
      .catch(error => {
         this.rootPage = LoginPage;
      });

  }

  // dummy auth, toggle true or false to enable login screen
  checkAuth(isLoggedIn: boolean) {
    return isLoggedIn; // dummy data
  }
}

ionicBootstrap(MyApp)
