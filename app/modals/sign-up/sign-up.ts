import { Component } from '@angular/core';
import { Modal, ViewController, NavController, Alert } from 'ionic-angular';
import { InAppBrowser } from 'ionic-native';

@Component({
	templateUrl: 'build/modals/sign-up/sign-up.html'
})
export class SignUpModal {

	user: any = {
		email: '',
		password: ''
	};

	constructor(
		private viewCtrl: ViewController,
		private nav: NavController
	) { }

	cancel() {
	    this.viewCtrl.dismiss(); 
	}

	openUrl(url) {
		InAppBrowser.open(url);
	}

	submit(user) {
		
		if(user.email == '') {
			
		} else {

		}
	}

	handleError(title, message) {
		let alert = Alert.create({
			title: title,
			message: message,
			buttons: ['Ok']
		});
	}
}